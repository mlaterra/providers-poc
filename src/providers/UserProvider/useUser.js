import { useContext } from 'react';
import UserContext from './UserContext';

export default () => {
  const context = useContext(UserContext);

  const {
    users,
    currentUser,
    selectUser,
  } = context;
  return {
    users,
    currentUser,
    selectUser,
  };
};
