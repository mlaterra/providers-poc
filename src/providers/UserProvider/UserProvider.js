import React, { useState, useEffect } from "react";
import UserContext from "./UserContext";
import PropTypes from "prop-types";
import { getUsers } from "../Handlers/UserHandler";

const UserProvider = ({ children }) => {
  const [users, setUsers] = useState({});
  const [currentUser, setCurrentUser] = useState({});

  const selectCurrentUser = (id) => {
    const found = users.find((user) => user.id === id);

    if (found) {
      setCurrentUser(found);
    }
  };

  const contextValue = {
    users,
    currentUser,
    selectUser: selectCurrentUser,
  };

  // Initial Load of Data
  useEffect(() => {
    const asyncCode = async () => {
      // Get data from API/Handlers
      const users = await getUsers();
      setUsers(users);
    };

    asyncCode();
  }, []);

  return React.createElement(
    UserContext.Provider,
    { value: contextValue },
    children
  );
};

UserProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.array,
    PropTypes.object,
  ]).isRequired,
};

UserProvider.displayName = "UserProvider";

export default UserProvider;
