const axios = require('axios');

export const getUsers = async () => {

    const config = {
        method: 'get',
        url: 'https://jsonplaceholder.typicode.com/users',
    }

    let res = await axios(config)
    return res.data;

} 