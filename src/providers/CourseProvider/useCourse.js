import { useContext } from 'react';
import CourseContext from './CourseContext';

export default () => {
  const context = useContext(CourseContext);

  const {
    courses,
  } = context;
  return {
    courses,
  };
};
