import React, { useState, useEffect } from 'react';
import CourseContext from './CourseContext';
import PropTypes from 'prop-types';
import {getCourses} from '../Handlers/CourseHandler';

const CourseProvider = ({ children }) => {
  const [courses, setCourses] = useState({});

  const contextValue = {
    courses,
  };

  // Initial Load of Data
  useEffect(() => {
    // Get data from API/Handlers
    const asyncCode = async () =>{
      const courses = await getCourses();
      setCourses(courses);
    }
    asyncCode();
  }, []);

  return React.createElement(
    CourseContext.Provider,
    { value: contextValue },
    children,
  );
};

CourseProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.array,
    PropTypes.object,
  ]).isRequired,
};

CourseProvider.displayName = "CourseProvider"

export default CourseProvider;
