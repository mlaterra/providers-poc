import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { useUser, UserProvider } from '../UserProvider';


describe('useProvider hook tests', () => {
  let userHook;

  beforeEach(async () => {
    // eslint-disable-next-line react/prop-types
    const wrapper = ({ children }) => <UserProvider>{children}</UserProvider>;
    userHook = renderHook(() => useUser(), { wrapper });

  });

  test('It loads users at mount', async () => {

    await userHook.waitForNextUpdate();

    expect(userHook.result.current.users).toHaveLength(10);
  });

  test('It selects the current user correctly', async () => {

    await userHook.waitForNextUpdate();

    act(() => {
      userHook.result.current.selectUser(1);
    })
    
    expect(userHook.result.current.currentUser.username).toBe('Bret');
    
  });
});
