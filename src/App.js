import React from "react";
import { useUser } from "./providers/UserProvider";
import { CourseProvider } from "./providers/CourseProvider";
import Course from "./Course";
import "./App.css";

const App = () => {
  const { users, currentUser, selectUser } = useUser();
  const handleClick = (id) => {
    selectUser(id);
  };

  return (
    <>
      <h2>{`user selected: ${
        currentUser.name ? currentUser.name : "none"
      }`}</h2>
      <hr />
      <br />
      <table style={{ border: "1px solid black" }}>
        {users.length &&
          users.map((item) => {
            return (
              <tbody key={item.id}>
                <tr
                  onClick={() => handleClick(item.id)}
                  style={{
                    background: currentUser.id === item.id ? "red" : "white",
                    cursor: "pointer",
                  }}
                >
                  <th style={{ border: "1px solid black" }}>{item.name}</th>
                  <th style={{ border: "1px solid black" }}>{item.username}</th>
                  <th style={{ border: "1px solid black" }}>{item.email}</th>
                </tr>
              </tbody>
            );
          })}
      </table>
      <CourseProvider>
        <Course />
      </CourseProvider>
    </>
  );
};

export default App;
