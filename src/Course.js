import React from "react";
import { useCourse } from "./providers/CourseProvider";

const Course = () => {

  const { courses } = useCourse();
  return (
    <>
      <div>
        <hr />
        <h2>Courses Section</h2>
        <br />
        {courses.length && courses.map((course) => {
          return <div key={course.id}>{course.title}</div>;
        })}
      </div>
    </>
  );
};

export default Course;
